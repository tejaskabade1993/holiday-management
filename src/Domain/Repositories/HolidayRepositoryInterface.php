<?php

namespace Domain\Repositories;

use Domain\Entities\Holiday;

interface HolidayRepositoryInterface {
    public function findByUserId(int $userId): array;
    public function findPendingRequests(): array;
    public function save(Holiday $holiday): void;
    // Add more methods as needed
}

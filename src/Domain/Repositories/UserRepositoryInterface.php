<?php

namespace Domain\Repositories;

use Domain\Entities\User;

interface UserRepositoryInterface {
    public function findById(int $id): ?User;
    public function findByUsername(string $username): ?User;
    // Add more methods as needed
}

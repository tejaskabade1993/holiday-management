<!-- Display a table of requested holidays -->
<table>
    <thead>
        <tr>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($holidayRequests as $holiday): ?>
            <tr>
                <td><?php echo $holiday->getStartDate(); ?></td>
                <td><?php echo $holiday->getEndDate(); ?></td>
                <td><?php echo $holiday->getStatus(); ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<!-- HTML form for requesting a holiday -->
<form action="submit_holiday_request.php" method="POST">
    <!-- Form fields for start date, end date, etc. -->
    <input type="date" name="start_date" required>
    <input type="date" name="end_date" required>
    <button type="submit">Request Holiday</button>
</form>

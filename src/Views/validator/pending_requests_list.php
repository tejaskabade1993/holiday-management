<!-- Display a list of pending holiday requests for validation -->
<ul>
    <?php foreach ($pendingRequests as $request): ?>
        <li>
            <a href="view_employee_request.php?id=<?php echo $request->getId(); ?>">
                <?php echo $request->getUser()->getUsername(); ?>'s Holiday Request
            </a>
        </li>
    <?php endforeach; ?>
</ul>

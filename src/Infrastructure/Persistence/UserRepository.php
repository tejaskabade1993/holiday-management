<?php

namespace Infrastructure\Persistence;

use Domain\User;
use Domain\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    public function findById($id)
    {
        // Code to fetch user from database by id
    }

    public function findByUsername(User $user)
    {
        // Code to persist user to database
    }

}

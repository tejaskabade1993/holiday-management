<?php

namespace Infrastructure\Persistence;
use Domain\Repositories\HolidayRepositoryInterface;


class HolidayRepository implements HolidayRepositoryInterface
{
    public function findByUserId($id)
    {
        try {
            $pdo = $this->getPdo(); // Example method to retrieve PDO instance
            $sql = "SELECT * FROM holidays WHERE user_id = :userId";
            $statement = $pdo->prepare($sql);
            $statement->bindParam(':userId', $userId, PDO::PARAM_INT);
            $statement->execute();
            $holidays = $statement->fetchAll(PDO::FETCH_ASSOC);
            if (count($holidays) > 0) {
                return $holidays;
            } else {
                return null; // No holidays found for the specified user ID
            }
        } catch (PDOException $e) {
            error_log('PDOException: ' . $e->getMessage());
            throw $e; // Rethrow the exception to propagate it up the call stack
        } catch (Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            return null; // Return null indicating an error occurred
        }
    }

    public function findPendingRequests(User $user)
    {
        // Code to persist user to database
    }

    public function save(Holiday $holiday)
    {
        try {
            // Assuming a database connection instance, e.g., $pdo
            $pdo = $this->getPdo(); // Example method to retrieve PDO instance
            $sql = "INSERT INTO holidays (user_id, start_date, end_date, status) 
                    VALUES (:userId, :startDate, :endDate, )";
            $statement = $pdo->prepare($sql);
    
            // Bind parameters
            $statement->bindValue(':userId', $holiday->getUserId(), PDO::PARAM_INT);
            $statement->bindValue(':startDate', $holiday->getStartDate(), PDO::PARAM_STR);
            $statement->bindValue(':endDate', $holiday->getEndDate(), PDO::PARAM_STR);
    
            // Execute the query
            $result = $statement->execute();
    
            // Check the result of the query
            if ($result) {
                return true; // Holiday saved successfully
            } else {
                return false; // Failed to save holiday
            }
        } catch (PDOException $e) {
            // Handle PDO exceptions (database errors)
            error_log('PDOException: ' . $e->getMessage());
           throw $e; 
        } catch (Exception $e) {
            error_log('Exception: ' . $e->getMessage());
            return false; 
        }
    }
}

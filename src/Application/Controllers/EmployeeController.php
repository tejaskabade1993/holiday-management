<?php

namespace Application\Controllers;

use Application\Services\EmployeeService;

class EmployeeController {
    private $employeeService;

    public function __construct(EmployeeService $employeeService) {
        $this->employeeService = $employeeService;
    }

    public function requestHoliday(int $userId, string $startDate, string $endDate): void {
        $this->employeeService->requestHoliday($userId, $startDate, $endDate);
    }

    public function viewHolidayRequests(int $userId): array {
        return $this->employeeService->getHolidayRequests($userId);
    }
}

<?php

namespace Application\Controllers;

use Application\Services\ValidatorService;

class ValidatorController {
    private $validatorService;

    public function __construct(ValidatorService $validatorService) {
        $this->validatorService = $validatorService;
    }

    public function viewPendingHolidayRequests(): array {
        return $this->validatorService->getPendingHolidayRequests();
    }

    public function viewEmployeeHolidayRequests(int $userId): array {
        return $this->validatorService->getEmployeeHolidayRequests($userId);
    }

    public function approveHolidayRequest(int $holidayId): void {
        $this->validatorService->approveHolidayRequest($holidayId);
    }

    public function denyHolidayRequest(int $holidayId): void {
        $this->validatorService->denyHolidayRequest($holidayId);
    }
}

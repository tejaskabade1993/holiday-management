<?php

namespace Application\Services;

use Domain\Repositories\HolidayRepositoryInterface;
use Domain\Entities\Holiday;

class EmployeeService {
    private $holidayRepository;

    public function __construct(HolidayRepositoryInterface $holidayRepository) {
        $this->holidayRepository = $holidayRepository;
    }

    public function requestHoliday(int $userId, string $startDate, string $endDate): void {
        // Validate and process holiday request
        $holiday = new Holiday(null, $userId, $startDate, $endDate, 'Pending');
        $this->holidayRepository->save($holiday);
    }

    public function getHolidayRequests(int $userId): array {
        return $this->holidayRepository->findByUserId($userId);
    }
}

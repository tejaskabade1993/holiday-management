<?php

namespace Application\Services;

use Domain\Repositories\HolidayRepositoryInterface;

class ValidatorService {
    private $holidayRepository;

    public function __construct(HolidayRepositoryInterface $holidayRepository) {
        $this->holidayRepository = $holidayRepository;
    }

    public function getPendingHolidayRequests(): array {
        return $this->holidayRepository->findPendingRequests();
    }

    public function getEmployeeHolidayRequests(int $userId): array {
        return $this->holidayRepository->findByUserId($userId);
    }

    /**
    * Logic to approve a holiday request 
    */ 
    public function approveHolidayRequest(int $holidayId): void {
        // Retrieve the holiday request from the repository
        $holiday = $this->holidayRepository->findById($holidayId);

        if ($holiday) {
            // Check if the holiday request is pending and can be approved
            if ($holiday->getStatus() === 'Pending') {
                // Update the status to 'Approved'
                $holiday->setStatus('Approved');
                // Save the updated holiday request back to the repository
                $this->holidayRepository->save($holiday);
                echo "Holiday request approved successfully.";
            } else {
                echo "Holiday request is not pending and cannot be approved.";
            }
        } else {
            echo "Holiday request not found.";
        }
    }

    /**
    * Logic to deny a holiday request 
    */ 
    public function denyHolidayRequest(int $holidayId): void {
        // Retrieve the holiday request from the repository
        $holiday = $this->holidayRepository->findById($holidayId);

        if ($holiday) {
            // Check if the holiday request is pending and can be denied
            if ($holiday->getStatus() === 'Pending') {
                // Update the status to 'Denied'
                $holiday->setStatus('Denied');
                // Save the updated holiday request back to the repository
                $this->holidayRepository->save($holiday);
                echo "Holiday request denied successfully.";
            } else {
                echo "Holiday request is not pending and cannot be denied.";
            }
        } else {
            echo "Holiday request not found.";
        }
    }
}

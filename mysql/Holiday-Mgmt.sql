
CREATE TABLE Users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL UNIQUE,
    surname VARCHAR(255) NOT NULL,
    department VARCHAR(255),
	password VARCHAR(255) NOT NULL,
    role ENUM('Employee', 'Validator') NOT NULL
);


CREATE TABLE Holidays (
    id INT PRIMARY KEY AUTO_INCREMENT,
    user_id INT NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    status ENUM('Pending', 'Approved', 'Denied') NOT NULL,
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Insert records into Users table
INSERT INTO Users (username, surname, department, password, role)
VALUES
    ('user1', 'surname1', 'HR', 'password123', 'Employee'),
    ('user2', 'surname2', 'Finance', 'securepwd456', 'Employee'),
    ('validator1', 'surname3', 'Marketing', 'topsecret789', 'Validator'),
    ('validator2', 'surname4', 'Marketing', 'topsecret789', 'Validator');

-- Insert holiday request for user with id = 1
INSERT INTO Holidays (user_id, start_date, end_date, status)
VALUES 
	(1, '2024-06-01', '2024-06-05', 'Pending'),
	(2, '2024-07-15', '2024-07-20', 'Pending'),
	(1, '2024-08-10', '2024-08-15', 'Pending'),
	(3, '2024-09-01', '2024-09-03', 'Pending');







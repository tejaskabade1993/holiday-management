
    $(function() {
        $("[data-toggle=popover]").popover({
            html: true,
            placement:"right",
            content: function() {
            var content = $(this).attr("data-popover-content");
            return $(content).children(".popover-body").html();
            },
            title: function() {
            var title = $(this).attr("data-popover-content");
            return $(title).children(".popover-heading").html();
            }
        });
    });
    function toggleVisibility(id) {
        const requestedHolidays = document.getElementById('requestedHolidays');
        requestedHolidays.classList.toggle('hidden');
    }

    function ShowSuccessMsg() {
        $('#customAlertMessage').fadeIn();
        // Hide the alert when the close button is clicked
        $('#customAlertMessage .close').click(function(){
            $('#customAlertMessage').fadeOut();
        });
    }

    $(document).ready(function () {
        //Search method
        $("#searchName, #searchDepartment").keyup(function () {
            search_table($(this).val());
        });
        function search_table(value) {
            $("#vlidatorViewTable tr").each(function () {
                var found = "false";
                $(this).each(function () {
                    if ($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0) {
                        found = "true";
                    }
                });
                if (found == "true") {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    });
      
    

    
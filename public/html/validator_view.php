<?php
use Application\Controllers\EmployeeController;
use Application\Controllers\ValidatorController;
?>

<div class="max-w-4xl mx-auto p-4 validatorsBox">
    <h1 class="text-xl font-bold mb-4">HOLIDAY TABLE</h1>
    <div class="flex gap-4 mb-6">
        <input type="text" placeholder="Search for name" class="border p-2 rounded" id="searchName">
        <input type="text" placeholder="Search for Department" class="border p-2 rounded" id="searchDepartment">
    </div>
    <table id="vlidatorViewTable" class="w-full text-left border-collapse">
        <thead>
            <tr>
                <th class="border-b p-2">Name</th>
                <th class="border-b p-2">Surname</th>
                <th class="border-b p-2">Department</th>
                <th class="border-b p-2">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="p-2">Erik</td>
                <td class="p-2">Smith</td>
                <td class="p-2">Accounting</td>
                <td class="p-2"><button data-toggle="popover" data-trigger="focus" data-popover-content="#a2">👁️</button></td>
            </tr>
            <tr>
                <td class="p-2">Paul</td>
                <td class="p-2">Cox</td>
                <td class="p-2">Accounting</td>
                <td class="p-2"><button data-toggle="popover" data-trigger="focus" data-popover-content="#a2">👁️</button></td>
            </tr>
            <tr>
                <td class="p-2">William</td>
                <td class="p-2">Terry</td>
                <td class="p-2">Product Analysis</td>
                <td class="p-2"><button data-toggle="popover" data-trigger="focus" data-popover-content="#a2">👁️</button></td>
            </tr>
            <tr>
                <td class="p-2">Mark</td>
                <td class="p-2">Spencer</td>
                <td class="p-2">Business & Services</td>
                <td class="p-2"><button data-toggle="popover" data-trigger="focus" data-popover-content="#a2">👁️</button></td>
            </tr>
            <tr>
                <td class="p-2">Erik</td>
                <td class="p-2">Smith</td>
                <td class="p-2">Software Development</td>
                <td class="p-2"><button data-toggle="popover" data-trigger="focus" data-popover-content="#a2">👁️</button>
            </td>
            </tr>
        </tbody>
    </table>
</div>
<!-- Popup over -->
<div id="a2" class="hidden">
    <div class="popover-heading">
        <h2 class="text-lg font-semibold">REQUESTED HOLIDAYS</h2>
    </div>
    <div class="popover-body">
        <ul class="list-disc pl-5">
            <li>From 11/02 to 13/02</li>
            <li>From 19/02 to 21/03</li>
            <li>From 03/04 to 08/04</li>
        </ul>
        <div class="flex gap-2 mt-2">
            <button class="bg-red-500 text-white px-4 py-2 rounded">Cancel</button>
            <button class="bg-blue-500 text-white px-4 py-2 rounded" onclick="ShowSuccessMsg()">Validate</button>
        </div>
    </div>
</div>
<!-- Custom alert message (initially hidden) -->
    <div id="customAlertMessage" class="alert alert-success alertMsg" style="display: none;">
        Success!
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>



